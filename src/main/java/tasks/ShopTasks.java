package tasks;

import pageobjects.HomePage;
import pageobjects.ShopPage;


import static constants.Constants.EMAIL;
import static constants.Constants.PASSWORD;

public class ShopTasks extends HomePage {

    ShopPage shopPage = new ShopPage();

    public void buyItens(){

        clickSignIn();
        fillemail(EMAIL);
        fillPassword(PASSWORD);
        submitLogin();
        shopPage.clickTshirtSection();
    }
}
