package pageobjects;

import base.AbstractPage;
import driver.DriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ShopPage extends AbstractPage {

    private WebDriver driver;
    private WebDriverWait wait;

    public ShopPage() {
        driver = DriverManager.getDriver();
        wait = new WebDriverWait(driver, 15);
    }

    @FindBy(xpath = "//*[@id='block_top_menu']/ul/li[3]/a")
    WebElement tshirtSectionButton;

    @FindBy(className = "quick-view")
    WebElement tshirtWindow;


    public void clickTshirtSection(){
        wait.until(ExpectedConditions.elementToBeClickable(tshirtSectionButton));
        tshirtSectionButton.click();
    }

    public void selectTshirt(){
        wait.until(ExpectedConditions.elementToBeClickable(tshirtWindow));
        tshirtWindow.click();
    }
}
