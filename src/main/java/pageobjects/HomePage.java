package pageobjects;

import driver.DriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import base.AbstractPage;


public class HomePage extends AbstractPage {

    private WebDriver driver;
    private WebDriverWait wait;

    public HomePage() {
        driver = DriverManager.getDriver();
        wait = new WebDriverWait(driver, 15);
    }

    @FindBy(xpath = "//*[@id='header']/div[2]/div/div/nav/div[1]/a")
    WebElement signinButton;

    @FindBy(id = "email")
    WebElement emailInput;

    @FindBy(id = "passwd")
    WebElement passwdInput;

    @FindBy(id = "SubmitLogin")
    WebElement submitLoginButton;

    @FindBy(xpath = "//*[@id='header']/div[2]/div/div/nav/div[1]/a/span")
    WebElement userLoggedName;

    protected void clickSignIn(){
        wait.until(ExpectedConditions.elementToBeClickable(signinButton));
        signinButton.click();
    }

    protected void fillemail(String email){
        wait.until(ExpectedConditions.visibilityOf(emailInput));
        emailInput.clear();
        emailInput.sendKeys(email);
    }

    protected void fillPassword(String password){
        wait.until(ExpectedConditions.visibilityOf(passwdInput));
        passwdInput.clear();
        passwdInput.sendKeys(password);
    }

    protected void submitLogin(){
        submitLoginButton.click();
    }
}
