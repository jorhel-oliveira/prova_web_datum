package base;

import driver.DriverManager;
import org.openqa.selenium.support.PageFactory;

public abstract class AbstractPage extends BaseTest {

    public AbstractPage() {
        PageFactory.initElements(DriverManager.getDriver(), this);
    }
}
