# web_test_selenium

This project aims to run UI tests using Selenium Web Driver and the Page Object project pattern.

To run tests runs `mvn test` command on the terminal.

The Chrome Driver file is in the directory: **src / test / resources /**

This version of Chrome Driver supports Google Chrome browsers in version **88.0.4324.96**.

In the HomePage and ShopPage classes there are mappings and manipulations of page elements and actions with those elements. 

The ShopTasks class contains steps that composes the test.

In the BaseTest class are the driver's initialization properties for before and after testing.

The AbstractPage class, only generates initialization of the elements.

In the DriverManager class are the basic driver actions that are used to start the driver and end the driver section.
